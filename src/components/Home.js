import React,{useState} from 'react'
import AppNavBar from './AppNavBar'
import '../App.css';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

function Home() {
  // const [getter, setter] = useState(initial value)
  const [value, setValue] = useState(0)
  const [isValueOverlap, setIsValueOverlap] = useState(false)

  const increment = (e) =>{
    if (value >= 30) {
      setIsValueOverlap(true)
      return console.log("tama na")
    } else {
      setValue(value + 1)
      return console.log("dagdag pa")
    }
    e.preventDefault()
  
  
  }

  const decrement = (e) =>{
    setValue(value - 1) 
  }

  return (
      <div className='position-relative'>
      <AppNavBar/>
    <div>Home</div>
    <p>{value}</p> 
    <button onClick={(e)=>decrement(e)}>- bawas</button>
    <button onClick={(e)=>increment(e)}>+ dagdag</button>
    {isValueOverlap == true ?   <p>Tigil na</p> : <p>Sige pa</p>}
    <div id="form">
    <Form>
  <Form.Group className="mb-3" controlId="formBasicEmail">
    <Form.Label>Title</Form.Label>
    <Form.Control type="text" placeholder="Enter Title.." />
  </Form.Group>

  <Form.Group className="mb-3" controlId="formBasicPassword">
    <Form.Label>Body</Form.Label>
    <Form.Control type="text" placeholder="Enter Body..." />
  </Form.Group>
  <Button variant="primary" type="submit">
    Submit
  </Button>
</Form>
</div>
    </div>

  )
}

export default Home